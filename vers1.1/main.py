#
# Zola Mahlaza
# Driver responsible for formatting ast
#
import slangparser
import sys

nline = ['Program','MainDecl','ArrayBrackets','ArrayBracketsSTAR','IdentList',
	'DeclList','DeclListSTAR','VarDecl','VarDeclSTAR','FuncDecl','FormalList',
	'FormalRest','FormalRestSTAR','Type','StatementSTAR','Statement','ExpRestSTAR',
	'ExpRest','Exp','ExpList','ID']

def process(tab,vtuple):
	for i in vtuple:
		if (isinstance(i, tuple) or isinstance(i,list)):
			process(tab,i)
		else:
			if (i != None):
				print(tab+str(i))
				if (i in nline):
					tab=tab+'\t'
				

filename = sys.argv[1]
data = ''
for line in (open(filename).readlines()):
	data = data+line
result = slangparser.parser.parse(data)
if (result!=None):
	process("",result)
