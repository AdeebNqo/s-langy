#
# Zola Mahlaza
# Responsible for parsing, defining the grammar, etc.
#
import slanglexer
import yacc
import sys

tokens = slanglexer.tokens
precedence = (

	('nonassoc','RSBRACE'),
	('nonassoc','RRBRACE'),
	('nonassoc','ELSE'),
	('right','EQUAL'),
	('nonassoc','SEMICOLON'),
	('left','OR'),
	('left','DAND'),
	('left','DEQUAL','NEQUAL'),
	('left','LESSTHAN','GREATERTHAN','RARROW','LARROW'),
	('left', '+', '-'),
	('left', '*', '/'),
	('left','!','PCENT'),
	('right','UPLUS','UMINUS')
)
#parsing functions
def p_error(t):
	if (t):
	    print('SyntaxError: invalid syntax\n\tUnexpected token \''+str(t.value)+'\' at line '+str(t.lineno))
	else:
	    print("Syntax error")
def p_Program(t):
	'''Program : MainDecl DeclListSTAR '''
	t[0] = ('Program',t[1],t[2])
def p_MainDecl(t):
	'''MainDecl : empty INT MAIN LRBRACE FormalList RRBRACE LCBRACE VarDeclSTAR StatementSTAR RCBRACE'''
	t[0] = ('MainDecl',t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9],t[10])

def p_ArrayBrackets(t):
	'''ArrayBrackets : LSBRACE INT_LITERAL RSBRACE
		| DSBRACE'''
	length = len(t)
	if (length==2):
	    t[0] = ('ArrayBrackets',t[1])
	elif (length==4):
	    t[0] = ('ArrayBrackets',t[1],t[2],t[3])
def p_ArrayBracketsSTAR0(t):
	'''ArrayBracketsSTAR :'''
	t[0]=('ArrayBracketsSTAR',[])
def p_ArrayBracketsSTAR(t):
	'''ArrayBracketsSTAR : ArrayBrackets ArrayBracketsSTAR'''
	t[0]=('ArrayBracketsSTAR',t[1],t[2])

def p_IdentList0(t):
	'''IdentList : ID ArrayBracketsSTAR'''
	t[0] = ('ID',t[1],t[2])
def p_IdentList(t):
	'''IdentList : IdentList COMMA ID ArrayBracketsSTAR'''
	t[0] = ('IdentList',t[1],t[2],t[3],t[4])

def p_DeclList(t):
	'''DeclList : VarDecl
		| FuncDecl'''
	t[0]=('DeclList',t[1])
	
def p_DeclListSTAR0(t):
	'''DeclListSTAR : empty'''
	t[0]=('DeclListSTAR',[])
def p_DeclListSTAR(t):
	'''DeclListSTAR : DeclList DeclListSTAR'''
	t[0] = ('DeclListSTAR',t[1],t[2])

def p_VarDecl0(t):
	'''VarDecl : Type IdentList SEMICOLON'''
	t[0] = ('VarDecl',t[1], t[2], t[3])

def p_VarDeclSTAR0(t):
	'''VarDeclSTAR :'''
	t[0]=('VarDeclSTAR',[])
def p_VarDeclSTAR(t):
	'''VarDeclSTAR : VarDecl VarDeclSTAR'''
	t[0]=('VarDeclSTAR',t[1],t[2])

def p_FuncDecl0(t):
	'''FuncDecl : Type ID LRBRACE FormalList RRBRACE LCBRACE VarDeclSTAR StatementSTAR RCBRACE'''
	t[0]=('FuncDecl',t[1],t[2],t[4],t[5],t[6],t[7],t[8],t[9])

def p_FormalList0(t):
	'''FormalList : VOID'''
	t[0] = ('FormalList',t[1])
def p_FormalList1(t):
	'''FormalList :'''
	t[0]=('FormalList',[])
def p_FormalList2(t):
	'''FormalList : Type ID ArrayBracketsSTAR FormalRestSTAR'''
	t[0]=('FormalList',t[1],t[2],t[3],t[4])	
	
def p_FormalRest0(t):
	'''FormalRest : Type ID ArrayBracketsSTAR'''
	t[0]=('FormalList',t[1],t[2],t[3])
	
def p_FormalRestSTAR0(t):
	'''FormalRestSTAR :'''
	t[0]=('FormalRestSTAR',[])
def p_FormalRestSTAR(t):
	'''FormalRestSTAR : FormalRest FormalRestSTAR'''
	t[0]=('FormalRestSTAR',t[1],t[2])
	
#########All the productions below work fine, exp is not finished though

def p_Type(t):
	'''Type : INT
		| FLOAT'''
	t[0]=('Type',t[1])

##Statement Productions

def p_StatementSTAR(t):
	'''StatementSTAR : Statement StatementSTAR
		| '''
	length = len(t)
	if (length==3):
		t[0] = ('StatementSTAR',t[1],t[2])
	elif (length==2):
		t[0] = ('StatementSTAR',[])

def p_Statement0(t):
	'''Statement : RETURN SEMICOLON'''
	t[0]=('Statement',t[1],t[2])
def p_Statement1(t):
	'''Statement : RETURN Exp SEMICOLON'''
	t[0]=('Statement',t[1],t[2],t[3])
def p_Statement2(t):
	'''Statement : ID LRBRACE ExpList RRBRACE SEMICOLON'''
	t[0]=('Statement',t[1],t[2],t[3],t[4],t[5])
def p_Statement3(t):
	'''Statement : ID LSBRACE Exp RSBRACE EQUAL Exp SEMICOLON'''
	t[0]=('Statement',t[1],t[2],t[3],t[4],t[5],t[6],t[7])
def p_Statement4(t):
	'''Statement : ID EQUAL Exp SEMICOLON'''
	t[0]=('Statement',t[1],t[2],t[3],t[4])
def p_StatementFor(t):
	'''Statement : FOR LRBRACE ExpList SEMICOLON ExpList SEMICOLON ExpList RRBRACE Statement'''
	t[0]=('Statement',t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9])
def p_StatementIF(t):
	'''Statement : IF LRBRACE Exp RRBRACE Statement'''
	t[0]=('Statement',t[1],t[2],t[3],t[4],t[5])

def p_StatementIFELSE(t):
	'''Statement : IF LRBRACE Exp RRBRACE Statement ELSE Statement'''
	t[0]=('Statement',t[1],t[2],t[3],t[4],t[5],t[6],t[7])

def p_Statement5(t):
	'''Statement : LCBRACE StatementSTAR RCBRACE'''
	t[0]=('Statement',t[1],t[2],t[3])

##ExpressionREst productions
def p_ExpRestSTAR(t):
	'''ExpRestSTAR : ExpRest ExpRestSTAR
			| '''
	length = len(t)
	if (length==3):
		t[0] = ('ExpRestSTAR',t[1],t[2])
	elif (length==2):
		t[0] = ('ExpRestSTAR',[])
def p_ExpRest(t):
	'''ExpRest : COMMA Exp'''
	t[0] = ('ExpRest',t[1],t[2])
#Expression productions
def p_expNUM(t):
	'''Exp : INT_LITERAL'''
	t[0] = ('Exp','(\'INT_LITERAL\', \''+str(t[1])+'\')')
def p_expNUM1(t):
	'''Exp : FLOAT_LITERAL'''
	t[0] = ('Exp','(\'FLOAT_LITERAL\', \''+str(t[1])+'\')')
def p_expID(t):
	'''Exp : ID'''
	t[0]=('Exp','(\'ID\', \''+str(t[1])+'\')')

def p_expBRACED(t):
	'''Exp : LRBRACE Exp RRBRACE'''
	t[0]=('Exp',t[1],t[2],t[3])
def p_expBinop(t):
	'''Exp : Exp '+' Exp
		| Exp '-' Exp
		| Exp '*' Exp
		| Exp '/' Exp'''
	t[0]=('Exp',t[1],t[2],t[3])
	#~ if t[2]=='+':
		#~ t[0]=t[1]+t[3]
	#~ if t[2]=='-':
		#~ t[0]=t[1]-t[3]
	#~ if t[2]=='*':
		#~ t[0]=t[1]*t[3]
	#~ if t[2]=='/':
		#~ t[0]=t[1]/t[3]
def p_expBRACE(t):
	'''Exp : ID LSBRACE Exp RSBRACE
		| ID LRBRACE Exp RRBRACE'''
	t[0] = ('Exp',t[1],t[2],t[3],t[4])
def p_expIdEqualExp(t):
	'''Exp : ID EQUAL Exp'''
	t[0] =('Exp',t[1],t[2],t[3])
def p_expEqualBracedExp(t):
	'''Exp : ID LSBRACE Exp RSBRACE EQUAL Exp'''
	t[0]=('Exp',t[1],t[2],t[3],t[5],t[6])
def p_expUminus(t):
	'''Exp : '-' Exp %prec UMINUS'''
	t[0]=('Exp',t[1],t[2])
def p_expUplus(t):
	'''Exp : '+' Exp %prec UPLUS'''
	t[0] = ('Exp',t[1],t[2])
def p_expNot(t):
	'''Exp : '!' Exp'''
	t[0] = ('Exp',t[1],t[2])
def p_expDEQUALPCENT(t):
	'''Exp : Exp DEQUAL Exp
		| Exp PCENT Exp'''
	t[0] = ('Exp',t[1],t[2],t[3])
def p_expDANDNEQUAL(t):
	'''Exp : Exp DAND Exp
		| Exp NEQUAL Exp'''
	t[0] = ('Exp',t[1],t[2],t[3])
def p_expLESSTHANGREATERTHAN(t):
	'''Exp : Exp LESSTHAN Exp
		| Exp GREATERTHAN Exp'''
	t[0] = ('Exp',t[1],t[2],t[3])
def p_expRARROW(t):
	'''Exp : Exp RARROW Exp'''
	t[0]=('Exp',t[1],t[2],t[3])
def p_exptLARROW(t):
	'''Exp : Exp LARROW Exp'''
	t[0]=('Exp',t[1],t[2],t[3])
def p_expOR(t):
	'''Exp : Exp OR Exp'''
	t[0]=('Exp',t[1],t[2],t[3])
def p_ExpList(t):
	'''ExpList : Exp ExpRestSTAR
		|'''
	t[0]=('ExpList',t[1],t[2])
def p_empty(t):
	'''empty :'''
#building parser
parser = yacc.yacc(start='Program');
