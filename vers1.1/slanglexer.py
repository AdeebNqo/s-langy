import lex

names = {}
#literals
literals=['=','-','+','/','*','!']
#reserved words
reserved = {
   'if' : 'IF',
   #'then' : 'THEN',
   #'while' : 'WHILE',
   'else' : 'ELSE',
   'void':'VOID',
   'float':'FLOAT',
   'main':'MAIN',
   'int':'INT',
   'for':'FOR',
   'return':'RETURN'
}

#tokens
tokens = [
	'ID',
	'INT_LITERAL',
	'FLOAT_LITERAL',
	#'WHITESPACE',
	#'COMMENT',
	'LARROW',
	'RARROW',
	'DEQUAL', #Double equal ==
	'NEQUAL', #Not equal
	'DAND', #Double and &&
	'OR',
	#'DRBRACE', #Double Round brace
	'DSBRACE', #Double Square brace
	#'DCBRACE',  #Double Curly brace
	"LRBRACE",
	"RRBRACE",
	'RCBRACE',
	'LCBRACE',
	'LSBRACE',
	'RSBRACE',
	'EQUAL',
	'COMMA',
	'SEMICOLON',
	'GREATERTHAN',
	'LESSTHAN',
	'PCENT'
] + list(reserved.values())


#regular expressions for the tokens
#t_COMMENT = r'(\/\/(.+)$)|(\/\*(\s|.)+\*\/)'
#t_WHITESPACE = r'\s+'
t_LESSTHAN=r'<'
t_GREATERTHAN=r'>'
t_LARROW = r'<='
t_RARROW = r'>='
t_DEQUAL = r'=='
t_NEQUAL = r'!='
t_DAND = r'&&'
t_OR = r'\|\|'
t_PCENT=r'%'
#t_DRBRACE = r'\(\)'
t_DSBRACE = r'\[]'
#t_DCBRACE = r'{}'
t_EQUAL=r'='
#t_DOT=r'\.'
t_COMMA=r','
t_SEMICOLON=r';'
t_RSBRACE=r'\]'
t_LSBRACE=r'\['
t_LCBRACE='{'
t_RCBRACE='}'
t_LRBRACE='\('
t_RRBRACE='\)'
#lexer functions
def t_error(token):
        #print("Illegal character '%s'" % token.value[0])
        token.lexer.skip(1)
def t_ID(t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        t.type = reserved.get(t.value,'ID')
        return t
def t_INT_LITERAL(t):
	r'-?\d+'
	t.value = int(t.value)
	return t
def t_FLOAT_LITERAL(t):
	r'(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'
	t.value = float(t.value)
	return t
#Tracking line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
slang_lexer = lex.lex()
if __name__ == "__main__":
    lex.runmain(lexer)
