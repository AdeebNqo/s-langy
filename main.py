import yacc
import lex
import sys

#reserved words
reserved=(
	'VOID',
	'FOR',
	'IF',
	'FLOAT',
	'RETURN',
	'ELSE',
	'MAIN'
)

#tokens
tokens = (
	'ID',
	'INT_LITERAL',
	'FLOAT_LITERAL',
	'WHITESPACE',
	'COMMENT',
	'LARROW',
	'RARROW',
	'DEQUAL', #Double equal ==
	'NEQUAL', #Not equal
	'DAND', #Double and &&
	'OR',
	'DRBRACE', #Double Round brace
	'DSBRACE', #Double Square brace
	'DCBRACE'  #Double Curly brace
	,"LRBRACE",
	"RRBRACE",
	'RCBRACE',
	'LCBRACE',
	'LSBRACE',
	'RSBRACE',
	'EQUAL',
	'STAR',
	'DOT',
	'COMMA',
	'SEMICOLON',
	'PCENT',
	'MINUS',
	'PLUS',
	'DIVIDE',
	'NOT',
	'RPOINTER',
	'LPOINTER'
) + reserved


#regular expressions for the tokens
t_COMMENT = r'(\/\/(.+)$)|(\/\*(\s|.)+\*\/)'
t_WHITESPACE = r'\s+'
t_INT_LITERAL = r'-?\d+'
t_FLOAT_LITERAL = r'-?\d+\.(\d+((e-|E-|E+|E-)\d+)?(f|F)?)?' #r'((\d+)\.(\d+)e-(\d+)(F|f))|((\d+)\.(\d+)f)|(\d+\.\d+)|(\.(\d+))|(\d+\.)'
t_LARROW = r'<='
t_RARROW = r'>='
t_DEQUAL = r'=='
t_NEQUAL = r'!='
t_DAND = r'&&'
t_OR = r'\|\|'
t_DRBRACE = r'\(\)'
t_DSBRACE = r'\[]'
t_DCBRACE = r'{}'
t_EQUAL=r'='
t_PLUS=r'\+'
t_DOT=r'\.'
t_STAR=r'\*'
t_COMMA=r','
t_SEMICOLON=r';'
t_PCENT=r'%'
t_MINUS=r'-'
t_DIVIDE=r'\/'
t_RSBRACE=r'\]'
t_LSBRACE=r'\['
t_LCBRACE='{'
t_RCBRACE='}'
t_LRBRACE='\('
t_RRBRACE='\)'
t_NOT=r'!'
t_RPOINTER=r'>'
t_LPOINTER=r'<'

#lexer functions
def t_error(token):
        print("Illegal character '%s'" % token.value[0])
        token.lexer.skip(1)

# Identifiers and reserved words
reserved_map = { }
for r in reserved:
    reserved_map[r.lower()] = r

def t_ID(t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        t.type = reserved_map.get(t.value,"ID")
        return t

#parsing functions
def p_error(t):
	print("Syntax error at "+t.value)

def p_Program(t):
	'''Program : DeclListSTAR MainDecl'''
def p_MainDecl(t):
	'''MainDecl :  Int MAIN LRBRACE FormalList RRBRACE LCBRACE VarDeclSTAR StatementSTAR RCBRACE'''
def p_Int(t):
	'''Int : INT'''
def p_ArrayBrackets(t):
	'''ArrayBrackets : LSBRACE INT_LITERAL RSBRACE
		| DSBRACE'''
def p_ArrayBracketsSTAR(t):
	'''ArrayBracketsSTAR : ArrayBrackets ArrayBracketsSTAR
		| '''
def p_IdentList(t):
	'''IdentList : IdentList COMMA ID ArrayBracketsSTAR
		| ID ArrayBracketsSTAR'''
def p_DeclList(t):
	'''DeclList : VarDecl
		| FuncDecl'''
def p_DeclListSTAR(t):
	'''DeclListSTAR : DeclList DeclListSTAR
		| '''
def p_VarDecl(t):
	'''VarDecl : Type IdentList SEMICOLON'''
def p_VarDeclSTAR(t):
	'''VarDeclSTAR : VarDecl VarDeclSTAR
		| '''
def p_FuncDecl(t):
	'''FuncDecl : Type ID LRBRACE FormalList RRBRACE LCBRACE VarDeclSTAR StatementSTAR RCBRACE'''
def p_FormalList(t):
	'''FormalList : Type ID ArrayBracketsSTAR FormalRestSTAR
		| VOID
		| '''
def p_FormalRest(t):
	'''FormalRest : COMMA Type ID ArrayBracketsSTAR'''
def p_FormalRestSTAR(t):
	'''FormalRestSTAR : FormalRest FormalRestSTAR
		| '''
def p_Type(t):
	'''Type : INT
		| FLOAT'''
def p_Statement(t):
	'''Statement : LCBRACE StatementSTAR RCBRACE
		| IF LRBRACE Exp RRBRACE Statement ELSE Statement
		| IF LRBRACE Exp RRBRACE Statement
		| FOR LRBRACE ExpList SEMICOLON ExpList SEMICOLON ExpList RRBRACE Statement
		| ID EQUAL Exp SEMICOLON
		| ID LSBRACE Exp RSBRACE EQUAL Exp SEMICOLON
		| RETURN Exp SEMICOLON
		| RETURN SEMICOLON
		| ID LRBRACE ExpList RRBRACE SEMICOLON'''
def p_StatementSTAR(t):
	'''StatementSTAR : Statement StatementSTAR
		| '''
def p_Exp(t):
	'''Exp : Exp binop Exp
		| ID EQUAL Exp
		| ID LSBRACE Exp RSBRACE EQUAL Exp
		| ID LSBRACE Exp RSBRACE 
		| ID LRBRACE Exp RSBRACE
		| INT_LITERAL 
		| FLOAT_LITERAL
		| ID
		| unop Exp
		| LRBRACE Exp RRBRACE'''
def p_ExpList(t):
	'''ExpList : Exp ExpRestSTAR
		| '''
def p_ExpRest(t):
	'''ExpRest : COMMA Exp'''
def p_ExpRestSTAR(t):
	'''ExpRestSTAR : ExpRest ExpRestSTAR
			| '''
def p_binop(t):
	'''binop : Exp PLUS Exp 
		| Exp MINUS Exp 
		| Exp STAR Exp 
		| Exp DIVIDE Exp 
		| Exp PCENT Exp
		| Exp LPOINTER Exp
		| Exp LARROW Exp
		| Exp RARROW Exp
		| Exp RPOINTER Exp
		| Exp EQUAL Exp
		| Exp DEQUAL Exp
		| Exp NEQUAL Exp
		| Exp DAND Exp
		| Exp OR Exp'''
def p_unop(t):
	'''unop : NOT Exp 
		| PLUS Exp
		| MINUS Exp'''
#building parser
parser = yacc.yacc();
try:
	lex.lex()
	#reading src code from file
	data = ""
	f = open(sys.argv[1])
	lines = f.readlines()
	for line in lines:
		data+=line
	output = parser.parse(data)
	print(output)
except IndexError:
	print("Please make sure you entered the filename")
